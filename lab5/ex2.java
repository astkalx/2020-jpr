import java.util.Scanner;

public class ex2 {
    public static void main(String[] args) throws Exception {
        String in_str;
        String result;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите строку:");
        in_str = scanner.nextLine();
        System.out.println(toCamelCase(in_str));
        System.out.println(to_snake_case(in_str));
    }

    private static String toCamelCase(String inp_str){
        char[] sym_arr = inp_str.toCharArray();
        String result = "" + sym_arr[0];

        for (int i = 1; i < sym_arr.length; i = i + 1) {
            if (sym_arr[i] == '_') continue;
            if (sym_arr[i-1] == '_')
                result = result + (char)(sym_arr[i] - 32);
            else
                result = result + sym_arr[i];
        }
        return result;
    }

    private static String to_snake_case(String inp_str){
        String result = "";

        for (char symb: inp_str.toCharArray()) {
            if (symb >= 'A' && symb <= 'Z') {
                result = result + '_';
                result = result + (char)(symb + 32);
            }
            else
                result = result + symb;
        }
        return result;
    }
}
