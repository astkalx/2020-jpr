import java.util.Scanner;

public class ex1 {
    public static void main(String[] args) throws Exception {
        String inp_str;
        int n;
        int k;

        Scanner scan = new Scanner(System.in);

        System.out.println("Введите n:");
        n = Integer.parseInt(scan.nextLine());
        System.out.println("Введите k:");
        k = Integer.parseInt(scan.nextLine());
        System.out.println("Введите строку:");
        inp_str = scan.next();

        proc(n, k, inp_str);
    }

    private static void proc(int n, int k, String inp_str) {
        String[] str_arr = inp_str.split(" ");
        String prnt_str = "";
        for (String wrd: str_arr) {
            if((prnt_str+wrd).length() <= k && prnt_str != "")
                prnt_str = prnt_str + wrd;
            else {
                System.out.println(prnt_str);
                prnt_str = wrd;
            }
        }
        System.out.println(prnt_str);
    }
}
