import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ex8 {
    public static void main(String[] args) throws Exception{
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("a:");
        int a = Integer.parseInt(input.readLine());
        System.out.println("b:");
        int b = Integer.parseInt(input.readLine());
        int c = nextEdge(a, b);
        System.out.println(c);
    }

    public static int nextEdge(int a, int b){
        return (a + b) - 1;
    }
}
