import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ex10 {
    public static void main(String[] args) throws Exception {
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("a:");
        int a = Integer.parseInt(input.readLine());
        System.out.println("b:");
        int b = Integer.parseInt(input.readLine());
        System.out.println("c:");
        int c = Integer.parseInt(input.readLine());

        boolean d = abcmath(a, b, c);
        System.out.println(d);
    }
    public static boolean abcmath(int a, int b, int c) {
        double s = a;
        for (int i=0; i<b; i=i+1) {
            s = s * 2;
        }
        if (s % c == 0)
            return true;
        else
            return false;
    }
}
