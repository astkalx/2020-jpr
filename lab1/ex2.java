import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ex2 {
    public static void main(String[] args) throws Exception{
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите высоту и основание:");
        double a = Double.parseDouble(input.readLine());
        double b = Double.parseDouble(input.readLine());
        double c = triArea(a, b);
        System.out.println(c);
    }

    public static double triArea(double a, double b){
        return (a * b) / 2;
    }
}
