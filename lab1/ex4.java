import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ex4 {
    public static void main(String[] args) throws Exception{
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Probability:");
        double a = Double.parseDouble(input.readLine());
        System.out.println("Prize:");
        double b = Double.parseDouble(input.readLine());
        System.out.println("Pay:");
        double d = Double.parseDouble(input.readLine());
        boolean c = profitableGamble(a, b, d);
        System.out.println(c);
    }

    public static boolean profitableGamble (double a, double b, double d){
        if(a * b - d > 1) return false;
            else return true;
    }
}
