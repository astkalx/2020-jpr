import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ex3 {
    public static void main(String[] args) throws Exception{
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Количество кур:");
        int a = Integer.parseInt(input.readLine());
        System.out.println("Количество коров:");
        int b = Integer.parseInt(input.readLine());
        System.out.println("Количество свиней:");
        int d = Integer.parseInt(input.readLine());
        int c = animals(a, b, d);
        System.out.println(c);
    }

    public static int animals(int a, int b, int d){
        return a * 2 + b * 4 + d * 4;
    }
}
