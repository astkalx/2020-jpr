import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ex9 {
    public static void main(String[] args) throws Exception {
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите количество чисел:");
        int a = Integer.parseInt(input.readLine());
        System.out.println("Заполните массив:");
        double [] arry = new double[a];
        for (int i=0; i<a; i=i+1){
            arry[i] = Double.parseDouble(input.readLine());
        }
        double c = sumOfCubes(arry);
        System.out.println(c);
    }

    public static double sumOfCubes(double[] arry) {
        double s = 0;
        for (int i=0; i<arry.length; i=i+1) {
            s = s + Math.pow(arry[i], 3);
        }
        return s;
    }
}
