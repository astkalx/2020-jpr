import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ex7 {
    public static void main(String[] args) throws Exception{
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("a:");
        int a = Integer.parseInt(input.readLine());
        double c = addUpTo(a);
        System.out.println(c);
    }

    public static double addUpTo(int a){
        return ((1 + a) / 2.0) * a;
    }
}
