import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ex5 {
    public static void main(String[] args) throws Exception{
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("a:");
        int a = Integer.parseInt(input.readLine());
        System.out.println("b:");
        int b = Integer.parseInt(input.readLine());
        System.out.println("N:");
        int d = Integer.parseInt(input.readLine());
        String c = operation(a, b, d);
        System.out.println();
    }

    public static String operation(int a, int b, int d){
        if(a-b==d) return "Вычитание";
        if(a+b==d) return "Сложение";
        if(a*b==d) return "Умножение";
        if(a/b==d) return "Деление";
        return "Ничего";
    }
}
