import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ex1 {
    public static void main(String[] args) throws Exception{
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите числа:");
        int a = Integer.parseInt(input.readLine());
        int b = Integer.parseInt(input.readLine());
        int c = remainder(a, b);
        System.out.println(c);
    }

    public static int remainder(int a, int b) {
        return a % b;
    }
}
