import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ex4 {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Введите текст");
        String t = reader.readLine();
        System.out.println(flipEndChars(t));
    }

    public static String flipEndChars(String t) {
        String flip = "";

        if (t.length() < 2) {
            flip = "Несовместимо.";
        }
        else if (t.charAt(t.length() - 1) == t.charAt(0)) {
            flip = "Два - это пара.";
        }
        else {
            char[] chars = t.toCharArray();
            char first = chars[0];
            chars[0] = chars[chars.length - 1];
            chars[chars.length - 1] = first;
            flip = new String(chars);
        }
        return flip;
    }
}
