import java.io.BufferedReader;
import java.io.InputStreamReader;

import java.util.Scanner;

public class ex1 {
    public static void main(String[] args) throws Exception {
        double a;
        double b;
        double c;
        double d;

        System.out.println("Введите коэффициенты a, b, c:");
        Scanner inp = new Scanner(System.in);
        a = inp.nextDouble();
        b = inp.nextDouble();
        c = inp.nextDouble();

        System.out.println(solution(a, b, c));
    }

    private static String solution(double a, double b, double c) {
        double d = b * b - 4 * a * c;
        if (d>0) {
            double x1;
            double x2;
            String answer = "";
            x1 = (-b - Math.sqrt(d)) / (2 * a);
            x2 = (-b + Math.sqrt(d)) / (2 * a);

            answer = "Уравнение имеет 2 корня:\nx1 = " + x1 + " x2 = " + x2 +"\n";
            return answer;
        }
        else if(d==0) {
            double x1;
            String answer = "";
            x1 = -b / (2 * a);
            answer = "Уравнение имеет один корень:\nx = " + x1 + "\n";
            return answer;
        }
        else return "Уравнение имеет только комплексные корни.\n";
    }
}
