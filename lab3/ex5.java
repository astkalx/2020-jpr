import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ex5 {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Введите текст");
        String t = reader.readLine();
        System.out.println(isValidHexCode(t));
    }

    public static boolean isValidHexCode(String t) {
        if(t.length()==7 && t.toCharArray()[0]=='#') {
            t = t.substring(1);
            for(char n: t.toCharArray()) {
                if (!(n >= '0' && n <= '9' || n >= 'A' && n<= 'F' || n >= 'a' && 'f' >= n))
                    return false;
                else
                    return true;
            }
        }
        return false;
    }
}
