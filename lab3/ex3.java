import java.util.Scanner;

public class ex3 {
    public static void main(String[] args) throws Exception {
        int n;
        System.out.println("Введите число:");
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        System.out.println(checkPerfect(n));
    }

    public static boolean checkPerfect(int n) {
        int sum = 0;
        for (int i=1; i<n; i=i+1) {
            if ((n%i)==0) {
                sum=sum+1;
            }
        }
        if(sum==n)
            return true;
        else
            return false;
    }
}
