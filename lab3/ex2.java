import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ex2 {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите текст:");
        String t = reader.readLine();
        System.out.println(findZip(t));
    }

    private static int findZip (String t) {
        int indexZip = t.indexOf("zip");
        indexZip = t.indexOf("zip", indexZip + 1);
        return indexZip;
    }
}
