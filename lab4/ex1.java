import java.util.Scanner;

public class ex1 {
    public static void main(String[] args) throws Exception {
        String in_str;
        int n;

        Scanner scan = new Scanner(System.in);
        System.out.println("Введите n:");
        n = Integer.parseInt(scan.nextLine());
        System.out.println(bell_number(n));
    }

    private static int fact(int n) {
        if (n == 0)
            return 1;
        else
            return n * fact(n-1);
    }

    private static int bell_number(int n) {
        int sum = 0;
        int cnk;
        if (n == 0)
            return 1;
        else {
            for (int k = 0; k < n; k = k + 1) {
                // System.out.println(fact(n-1));
                // System.out.println((fact(k)*fact(n-k-1)));
                cnk = fact(n-1)/(fact(k)*fact(n-k-1));
                sum = sum + (cnk * bell_number(k));
            }
            return sum;
        }
    }
}
