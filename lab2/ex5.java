import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ex5 {
    public static void main(String[] args) throws Exception {
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Число:");
        String t = input.readLine();
        String[] splitter = String.valueOf(t).split("\\.");
        System.out.println(getDecimalPlaces(splitter));
    }

    private static int getDecimalPlaces(String[] splitter) {
        if (splitter.length > 1) {
            return splitter[1].length();
        }
        else
            return 0;
    }
}
