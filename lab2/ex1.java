import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ex1 {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Введите слово:");
        String t = reader.readLine();
        System.out.println("Введите число повторений:");
        int n = Integer.parseInt(reader.readLine());

        System.out.println(repeat(t, n));
    }

    private static String repeat(String input_string, int count) {
        String work_string = "";
        for (char symbol:
            input_string.toCharArray()) {
            for (int i=0; i<count; i=i+1) {
                work_string = work_string + symbol;
            }
        }
        return work_string;
    }
}
