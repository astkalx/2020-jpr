import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ex3 {
    public static void main(String[] args) throws Exception {
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Число чисел в массиве:");
        int a = Integer.parseInt(input.readLine());
        System.out.println("Элементы массива:");
        int[] arry = new int[a];

        for (int i=0; i<a; i = i + 1)
            arry[i] = Integer.parseInt(input.readLine());
        System.out.println(isAW(arry));
    }

    public static boolean isAW(int[] arrz) {
        int sum = 0;
        int a = arrz.length;
        for (int i=0; i<arrz.length; i = i+1)
            sum = sum + arrz[i];
        if (sum % a == 0)
            return true;
        else
            return false;
    }
}
