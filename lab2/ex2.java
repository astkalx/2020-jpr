import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ex2 {
    public static void main(String[] args) throws Exception {
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Число чисел в массиве:");
        int a = Integer.parseInt(input.readLine());
        System.out.println("Элементы массива:");
        int[] arry = new int[a];

        for (int i=0; i<a; i = i + 1)
            arry[i] = Integer.parseInt(input.readLine());
        int c = differenceMaxMin(arry);
        System.out.println(c);
    }

    private static int differenceMaxMin(int[] arrz) {
        int min = arrz[0];
        int max = arrz[0];
        for (int number:
            arrz) {
            if (number<min) min = number;
            if (number>max) max = number;
        }
        return max - min;
    }
}
