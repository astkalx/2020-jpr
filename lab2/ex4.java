import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ex4 {
    public static void main(String[] args) throws Exception {
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Число чисел в массиве:");
        int a = Integer.parseInt(input.readLine());
        System.out.println("Элементы массива:");
        int[] arry = new int[a];

        for (int i=0; i<a; i = i + 1)
            arry[i] = Integer.parseInt(input.readLine());
        int[] arrn = cumulativeSum(arry);
        for (int i=0; i<a; i=i+1)
            System.out.println(arrn[i]);
    }

    public static int[] cumulativeSum(int[] arrz) {
        int sum = 0;
        int[] arrx = new int[arrz.length];
        for (int i=0; i<arrz.length; i=i+1) {
            sum = sum + arrz[i];
            arrx[i] = sum;
        }
        return arrx;
    }
}
